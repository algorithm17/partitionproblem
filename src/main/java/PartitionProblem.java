
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class PartitionProblem {

    static boolean partition(int[] ar, int i, int[] target, List<List<Integer>> p) {

        for (int k = 0; k < target.length && i < ar.length; k++) {
            if (target[k] >= ar[i]) {
                target[k] -= ar[i];

                if (partition(ar, i + 1, target, p)) {
                    p.get(k).add(ar[i]);
                    break;
                } else {

                    target[k] += ar[i];
                }
            }
        }
        return sum(target) == 0;
    }

    static int sum(int[] target) {
        int total = 0;
        for (int i = 0; i < target.length; i++) {
            total += target[i];
        }
        return total;
    }
}
