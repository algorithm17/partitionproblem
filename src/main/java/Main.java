
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class Main {

    private static boolean Check(int[] ar, int numPartitions) {
        int sum = PartitionProblem.sum(ar);

        if (sum % numPartitions != 0) {
            System.out.println("total sum " + sum + " is not divisibly by " + numPartitions + " subset");
            return false;
        }

        final int targetValue = sum / numPartitions;

        List<List<Integer>> p = new ArrayList<>();
        int[] target = new int[numPartitions];
        
        for (int t = 0; t < target.length; t++) {
            target[t] = targetValue;
            p.add(new ArrayList<>());
        }

        boolean isPossible = PartitionProblem.partition(ar, 0, target, p);
        System.out.println("Is partition possible = " + isPossible + " " + p);
        return isPossible;
    }

    public static void main(String[] args) {
        PartitionProblem A = new PartitionProblem();
        int ar[] = {3, 1, 5, 9, 12};
        int numPartition = 2;
        Check(ar, numPartition);

    }
}
